from random import randint

class HexGame:
    """
    Consumes randomizer function that always returns int and consumes two ints
    that define a range. For exmple `randomizer(0, 16) -> 2`. random.randint
    is ok.

    Every public method returns a list of strings
    """
    def __init__(self, randomizer):
        self.r = randomizer
        self.state = False
        self.question = ""

    def __answer(self, answer):
        try:
            answer = int(answer, 16)
            question = self.question
            question = question.split(' ')
            question = int(question[0], 16) + int(question[2], 16)
            if answer == question:
                return "Correct!"
            else:
                return "Wrong."
        except:
            return "Invalid input :/"

    def __ask(self):
        self.question = "{0} + {1}".format(
                hex(self.r(0, 16)),
                hex(self.r(0, 16)))
        return self.question

    def __remind(self):
        if self.state == True:
            return "The question was: {0}?".format(self.question)
        else:
            return "The game is not started"

    def remind(self):
        """Remind what the question was"""
        return [self.__remind()]

    def start(self):
        """Start the game"""
        if self.state == True:
            return ["The game has already started", self.__remind()]
        else:
            self.state = True
            return ["The game is started", "{0}?".format(self.__ask())]

    def stop(self):
        """Stop the game"""
        if self.state == True:
            self.state = False
            return ["The game has stopped"]
        else:
            return ["The game is not started"]

    def take_answer(self, answer):
        """
        You provide an answer as a string - it checks it correctness and asks a
        new one in case if answer was correct
        """
        if self.state != True:
            return ["The game is not started"]

        answer = self.__answer(answer);

        if answer == "Correct!":
            question = self.__ask()
            return [answer, "New question: {0}?".format(question)]

        return [answer]

class Chat:
    def __init__(self):
        self.hex_game = HexGame(randint)

class BotDB(dict):
    def get(self, chat_id):
        if self.__contains__(chat_id) == False:
            chat = Chat();
            self.__setitem__(chat_id, chat)
        else:
            chat = self.__getitem__(chat_id)

        return chat

