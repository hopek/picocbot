import unittest
from botdb import HexGame, BotDB
from random import randint

def randomizer_mock(start, end):
    return 0;

class TestHexGame(unittest.TestCase):
    def test_intantiation(self):
        game1 = HexGame(randomizer_mock)
        game2 = HexGame(randint)

    def test_start(self):
        game = HexGame(randomizer_mock)
        self.assertEqual(game.start(), ["The game is started", "0x0 + 0x0?"])

    def test_start_already_started(self):
        game = HexGame(randomizer_mock)
        game.start();
        self.assertEqual(game.start(),[
            "The game has already started",
            "The question was: 0x0 + 0x0?"
            ])

    def test_stop(self):
        game = HexGame(randomizer_mock)
        self.assertEqual(game.stop(), ["The game is not started"])

    def test_start_stop(self):
        game = HexGame(randomizer_mock)
        self.assertEqual(game.start(), ["The game is started", "0x0 + 0x0?"])
        self.assertEqual(game.stop(), ["The game has stopped"])

    def test_answer_game_is_not_started(self):
        game = HexGame(randomizer_mock)
        self.assertEqual(game.take_answer("0"), ["The game is not started"])

    def test_answer_correct(self):
        game = HexGame(randomizer_mock)
        game.start()
        self.assertEqual(
                game.take_answer("0"),
                ["Correct!", "New question: 0x0 + 0x0?"])

    def test_answer_wrong(self):
        game = HexGame(randomizer_mock)
        game.start()
        self.assertEqual(game.take_answer("ff"), ["Wrong."])

    def test_answer_invalid(self):
        game = HexGame(randomizer_mock)
        game.start()
        self.assertEqual(game.take_answer("mde"), ["Invalid input :/"])

    def test_remind_game_is_not_started(self):
        game = HexGame(randomizer_mock)
        self.assertEqual(game.remind(), ["The game is not started"])

    def test_remind_(self):
        game = HexGame(randomizer_mock)
        game.start();
        self.assertEqual(game.remind(), ["The question was: 0x0 + 0x0?"])

class TestBotDB(unittest.TestCase):
    def test_intantiation(self):
        bot = BotDB()

    def test_get(self):
        bot = BotDB()
        self.assertNotEqual(bot.get(1), None)

if __name__ == '__main__':
    unittest.main()
