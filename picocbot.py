from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
import logging
import os

from botdb import BotDB

updater = Updater(token=os.environ['TGTOKEN'])
dispatcher = updater.dispatcher

logging.basicConfig(
        format='%(asctiome)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

db = BotDB()

def bot_start(bot, update):
    chat_id = update.message.chat_id
    for msg in db.get(chat_id).hex_game.start():
        bot.send_message(chat_id=chat_id, text=msg)

def bot_stop(bot, update):
    chat_id = update.message.chat_id
    for msg in db.get(chat_id).hex_game.stop():
        bot.send_message(chat_id=chat_id, text=msg)

def bot_answer(bot, update):
    chat_id = update.message.chat_id
    answer = update.message.text.split(' ')[1]
    for msg in db.get(chat_id).hex_game.take_answer(answer):
        bot.send_message(chat_id=chat_id, text=msg)

def bot_remind(bot, update):
    chat_id = update.message.chat_id
    for msg in db.get(chat_id).hex_game.remind():
        bot.send_message(chat_id=chat_id, text=msg)

def bot_echo(bot, update):
    chat_id = update.message.chat_id
    print(update.message.text)
    bot.send_message(chat_id=chat_id, text=update.message.text)

def bot_help(bot, update):
    chat_id = update.message.chat_id
    handlers = ""
    for h in picoc_handlers:
        handlers += "/{0} - {1}\n".format(h[0], h[2])
    bot.send_message(chat_id=chat_id, text=handlers)

picoc_handlers = []
picoc_handlers.append(('start',     bot_start,  'Start "learning hex" game'))
picoc_handlers.append(('stop',      bot_stop,   'Stop "learning hex" game'))
picoc_handlers.append(('a',         bot_answer, 'Provide an answer in "learning hex" game'))
picoc_handlers.append(('remind',    bot_remind, 'Remind the question in "learning hex" game'))
picoc_handlers.append(('echo',      bot_echo,   'Echo for testing'))
picoc_handlers.append(('help',      bot_help,   'Get help on available commands'))

for h in picoc_handlers:
    dispatcher.add_handler(CommandHandler(h[0], h[1]))

updater.start_polling()
